package ru.testing.gateway;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.SneakyThrows;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


public class YandexTranslateGateway {
    private static final String URL = "https://api.weather.yandex.ru/v1/informers";
    private static final String TOKEN = "";

    @SneakyThrows
    public String getTranslate() throws IOException {
        URL url = new URL("https://translate.api.cloud.yandex.net/translate/v2/translate");
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setRequestProperty("Accept", "application/json");
        http.setRequestProperty("Authorization", "Bearer t1.9euelZrPk8eRzJDOyJmbiY3JkceNkO3rnpWancucmcfGzMjHjI3Lx8yLm5jl8_caG1B3-e9SUWZt_N3z91pJTXf571JRZm38.b5cBhLbrWE5Fywk3P1QwW4VPqHwaFqgWeA2UmvceoDFY7_VGGUKRmeX6y30HQ56cqfQfLAY0opLGqIi0Aj-mAQ");
        http.setRequestProperty("Content-Type", "application/json");

        String data = "{\n    \"folderId\": \"b1g4qlh51c71vdam35ca\",\n    \"texts\": [\"Hello World!\"],\n    \"targetLanguageCode\": \"ru\"\n}";

        byte[] out = data.getBytes(StandardCharsets.UTF_8);

        OutputStream stream = http.getOutputStream();
        stream.write(out);

        int resp = http.getResponseCode();

        if (resp == 200) {
            JsonObject jobj = new JsonParser().parse(new InputStreamReader(http.getInputStream())).getAsJsonObject();
            String result = jobj.toString().substring(26,47);
            http.disconnect();
            return result;
        } else {
            http.disconnect();
            return  "Error. Site response non 200. Response " + resp;
        }
    }
}
