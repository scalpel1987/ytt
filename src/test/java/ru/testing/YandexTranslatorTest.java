package ru.testing;

import org.junit.jupiter.api.DisplayName;
import ru.testing.gateway.YandexTranslateGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class YandexTranslatorTest {
    private static final String TRANSLATE_ANSWER = "Привет, мир!";

    @Test
    @DisplayName("Перевод Hellow World!")
    public void getHWTranslate() throws IOException {
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        String hwTranslation = yandexTranslateGateway.getTranslate();
        Assertions.assertEquals(hwTranslation, TRANSLATE_ANSWER);
    }

}
